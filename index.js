
const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const uuid = require('uuid').v4;

const port = process.env.PORT || 9000;

app.get('/', (req, res) => {
  res.json({ msg: 'It works!' });
});

io.on('connection', socket => {
  console.log('connected on server!');

  socket.on('chat-message', text => {
    console.log('Received chat-message: ', text);
    const id = uuid();
    io.emit('chat-message', { text, id });
  });
});

http.listen(port, () => {
  console.log('listening on *:' + port);
});
